module Types where

import Vector
import Data.Monoid

data Screen = Screen { width :: Int, height :: Int } deriving (Show)

class HasPos a where
    pos :: a -> Vector

data Camera = Camera { camPos :: Vector, camU :: Vector, camV :: Vector }
           deriving (Show)

mkCamera :: Vector -> Vector -> Vector -> Camera
mkCamera p u v = Camera p (normalize u) (normalize v)

instance HasPos Camera where
    pos = camPos

data Color = Color { red :: !Double, green :: !Double, blue :: !Double } deriving (Show)

colMulS :: Double -> Color -> Color
colMulS a (Color r g b) = Color (a * r) (a * g) (a * b)

colMul :: Color -> Color -> Color
colMul (Color r g b) (Color r' g' b') = Color (r * r') (g * g') (b * b')

colAdd :: Color -> Color -> Color
colAdd (Color r g b) (Color r' g' b') = Color (bound r r') (bound g g') (bound b b')
        where
            bound x x' = let res = (x + x')
                         in if res > 1 then 1 else res

instance Monoid Color where
    mempty = black
    mappend = colAdd

mkCol :: Int -> Int -> Int -> Color
mkCol r g b = Color (fromIntegral r / 255.0) (fromIntegral g / 255.0) (fromIntegral b / 255.0)

black :: Color
black = Color 0 0 0

white :: Color
white = Color 1 1 1

class HasColor a where
    color :: a -> Color

data Material = Material
        { diff :: Double
        , refl :: Double
        , spec :: Double
        , shin :: Double
        , refr :: Double
        , opac :: Double
        , col :: Color }
        deriving (Show)

instance HasColor Material where
    color = col

data Object = Sphere
        { radius :: Double
        , spherePos :: Vector
        , sphereMat :: Material }
            | Plane
        { planeNormal :: Vector
        , planeD :: Double
        , planeMat :: Material }
            | Triangle
        { tria :: Vector
        , trib :: Vector
        , tric :: Vector
        , triMat :: Material }
        deriving (Show)

material :: Object -> Material
material (Sphere _ _ m) = m
material (Plane _ _ m) = m
material (Triangle _ _ _ m) = m

mkPlane :: Vector -> Double -> Material -> Object
mkPlane v d m = Plane (normalize v) d m

instance HasPos Object where
    pos (Sphere _ p _) = p
    pos a = error $ show a ++ " has no position"

instance HasColor Object where
    color (Sphere _ _ m) = color m
    color (Plane _ _ m) = color m
    color (Triangle _ _ _ m) = color m

data Light = PosLight Vector Color
           | DirLight Vector Color
           | AmbLight Color
           deriving (Show)

mkDirLight :: Vector -> Color -> Light
mkDirLight v c = DirLight (normalize v) c

mkAmbLight :: Color -> Light
mkAmbLight c = AmbLight c

instance HasPos Light where
    pos (PosLight p _) = p
    pos _ = undefined

instance HasColor Light where
    color (PosLight _ c) = c
    color (DirLight _ c) = c
    color (AmbLight c) = c

data Scene = Scene
        { screen :: Screen
        , camera :: Camera
        , objects :: [Object]
        , lights :: [Light] }
        deriving (Show)

data Intersection = Inter
        { obj :: Object
        , dist :: Double }

instance Eq Intersection where
        (Inter _ x) == (Inter _ x') = x == x'

instance Ord Intersection where
        compare (Inter _ x) (Inter _ x') = compare x x'

setScreen :: Screen -> Scene -> Scene
setScreen scr s = s { screen = scr }

setCamera :: Camera -> Scene -> Scene
setCamera cam s = s { camera = cam }

addObject :: Object -> Scene -> Scene
addObject obj s = s { objects = obj:objects s }

addLight :: Light -> Scene -> Scene
addLight li s = s { lights = li:lights s }

emptyScene :: Scene
emptyScene = Scene
        { screen = Screen 800 600
        , camera = Camera (Vector 0 0 (-10)) (Vector 0 0 1) (Vector 0 1 0)
        , objects = []
        , lights = [] }

data Ray = Ray { orig :: Vector, dir :: Vector }
