{-# LANGUAGE MultiWayIf #-}
module Sphere where

import Maths
import Types
import Vector

sphereIntersect :: Object -> Ray -> Maybe Intersection
sphereIntersect this@(Sphere r p _) (Ray orig dir) =
            case inter of
                [] -> Nothing
                [x1, x2] -> Just $ Inter this (min x1 x2)
                [x] -> Just $ Inter this x
        where
            diff = orig #- p
            inter = roots 1 (2 * (dir #. diff)) ((diff #. diff) - r * r)

