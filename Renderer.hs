{-# LANGUAGE MultiWayIf #-}
module Renderer where

import Types
import Sphere
import Vector
import Data.Maybe
import Codec.Picture
import Data.Foldable (fold)

fovDeg :: Double
fovDeg = 45.0

fov :: Double
fov = (fovDeg / 180.0) * pi

genImg :: Pixel a => Int -> Int -> (Int -> Int -> a) -> Image a
genImg w h f = generateImage f w h

pix :: Color -> PixelRGB8
pix (Color r g b) = res `seq` res
        where
            res = PixelRGB8 (truncate $ r * 255) (truncate $ g * 255) (truncate $ b * 255)

for :: [a] -> (a -> b) -> [b]
for = flip map

planeIntersect :: Object -> Ray -> Maybe Intersection
planeIntersect obj@(Plane normal d _) (Ray orig dir) =
        denom >>= return . negate . (((normal #. orig) + d) /) >>= isPositive >>=
        mkIntersect
    where
        denom = let res = normal #. dir in if res >= 0 then Nothing else Just res
        isPositive x = if x >= 0 then Just x else Nothing
        mkIntersect x = Just $ Inter obj x
planeIntersect _ _ = undefined

enlight :: Scene -> Ray -> Intersection -> Color
enlight s (Ray orig dir) (Inter obj x) = colMul (color m) . fold . for (lights s) $ \l ->
        case l of
            AmbLight c -> c
            DirLight dir c ->
                    let incidence = normal #. dir
                    in  if incidence > 0
                        then colMulS (diff m * incidence) c
                        else black
            PosLight p c ->
                    let toLight = p #- pos
                        incidence = normalize toLight #. normal
                        dist = vlength toLight
                        attenuation = 1 / (dist * dist)
                    in  if incidence > 0
                        then colMulS (attenuation * diff m * incidence) c
                        else black
    where
        pos = (x #* dir) #+ dir
        m = material obj
        normal = computeNormal obj (orig #+ (dir #* x))

computeNormal :: Object -> Vector -> Vector
computeNormal (Sphere _ p _) inters = normalize $ p #- inters
computeNormal (Plane n _ _) _ = n
computeNormal (Triangle a b c _) _ = normalize $ (a #- b) `cross` (a #- c)

cast :: Scene -> Ray -> Color
cast s r = fold . for (objects s) $ \obj ->
    case obj of
        sph@(Sphere _ _ _) -> fromMaybe black $ sphereIntersect sph r >>= Just . enlight s r
        pla@(Plane _ _ _) -> fromMaybe black $ planeIntersect pla r >>= Just. enlight s r
        _ -> black

render :: Scene -> Image PixelRGB8
render s =
        genImg (width scr) (height scr) $ \x' y' ->
            let x = wCenter - x'
                y = hCenter - y'
                goal = (x #* u) #+ (y #* v) #+ center
                ray = Ray (pos cam) (normalize goal)
            in
                pix $ cast s ray
    where
        scr = screen s
        cam = camera s
        u = normalize $ camU cam
        v = normalize $ camV cam
        ortho = cross u v
        l = (fromIntegral (width scr) / 2) / tan (fov / 2)
        center = pos cam #+ (l #* ortho)
        wCenter = width scr `div` 2
        hCenter = height scr `div` 2

