{-# LANGUAGE MultiWayIf #-}
module Maths where

roots :: Double -> Double -> Double -> [Double]
roots a b c = if | det > 0 -> [(-b - sqrt det) / (2 * a),
                               (-b + sqrt det) / (2 * a)]
                 | det == 0 -> [(-b - sqrt det) / (2 * a)]
                 | otherwise -> []
    where
        det :: Double
        det = b * b - 4 * a * c

