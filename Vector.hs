{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE BangPatterns #-}

module Vector where

data Vector = Vector { x :: !Double, y :: !Double, z :: !Double } deriving (Show)

dot :: Vector -> Vector -> Double
dot (Vector x1 y1 z1) (Vector x2 y2 z2) = x1 * x2 + y1 * y2 + z1 * z2

(#.) = dot

cross :: Vector -> Vector -> Vector
cross (Vector x1 y1 z1) (Vector x2 y2 z2) =
        Vector (y1 * z2 - z1 * y2) (z1 * x2 - x1 * z2) (x1 * y2 - y1 * x2)

vlength :: Vector -> Double
vlength (Vector x y z) = sqrt (x * x + y * y + z * z)

normalize :: Vector -> Vector
normalize v = v #/ vlength v

angle :: Vector -> Vector -> Double
angle v1 v2 = acos $ (v1 #. v2) / (vlength v1 * vlength v2)

class LinAdd a b where
    (#+) :: a -> b -> Vector
    (#-) :: a -> b -> Vector

class LinMul a b where
    (#*) :: a -> b -> Vector

instance LinAdd Vector Vector where
    (Vector x1 y1 z1) #+ (Vector x2 y2 z2) = Vector (x1 + x2) (y1 + y2) (z1 + z2)
    (Vector x1 y1 z1) #- (Vector x2 y2 z2) = Vector (x1 - x2) (y1 - y2) (z1 - z2)

instance LinAdd Vector Double where
    (Vector x1 y1 z1) #+ f = Vector (x1 + f) (y1 + f) (z1 + f)
    (Vector x1 y1 z1) #- f = Vector (x1 - f) (y1 - f) (z1 - f)

instance LinAdd Double Vector where
    f #+ (Vector x1 y1 z1) = Vector (x1 + f) (y1 + f) (z1 + f)
    f #- (Vector x1 y1 z1) = Vector (x1 - f) (y1 - f) (z1 - f)

instance LinMul Vector Double where
    (Vector x1 y1 z1) #* f = Vector (x1 * f) (y1 * f) (z1 * f)

instance LinMul Double Vector where
    f #* (Vector x1 y1 z1) = Vector (x1 * f) (y1 * f) (z1 * f)

instance LinMul Int Vector where
    f #* v = (fromIntegral f :: Double) #* v

instance LinMul Vector Int where
    v #* f = v #* (fromIntegral f :: Double)

(Vector x y z) #/ f = Vector (x / f) (y / f) (z / f)
