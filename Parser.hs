module Parser where

import Types
import Vector
import Control.Applicative
import Control.Monad.State

getOne :: State [a] a
getOne = do
    h <- get
    modify tail
    return $ head h

int :: State [String] Int
int = read <$> getOne

double :: State [String] Double
double = read <$> getOne

parseScene :: [Char] -> Scene
parseScene = foldr (fst . runState parseLine) emptyScene . map words . lines
    where
        parseLine :: State [String] (Scene -> Scene)
        parseLine = do
            obj <- getOne
            case obj of
                "screen" -> setScreen <$> (Screen <$> int <*> int)
                "camera" -> setCamera <$> (mkCamera <$> vec <*> vec <*> vec)
                "sphere" -> addObject <$> (Sphere <$> double <*> vec <*> mat)
                "plane" -> addObject <$> (mkPlane <$> vec <*> double <*> mat)
                "triangle" -> addObject <$> (Triangle <$> vec <*> vec <*> vec <*> mat)
                "plight" -> addLight <$> (PosLight <$> vec <*> col)
                "dlight" -> addLight <$> (mkDirLight <$> vec <*> col)
                "alight" -> addLight <$> (mkAmbLight <$> col)
                _ -> pure id

        vec = Vector <$> double <*> double <*> double
        col = mkCol <$> int <*> int <*> int
        mat = Material <$> double <*> double <*> double <*> double <*> double <*> double <*> col
