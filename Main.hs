module Main where

import Parser
import Renderer
import System.Environment
import Codec.Picture

main = do
    args <- getArgs
    file <- readFile $ args !! 0
    let parsed = parseScene file
    print parsed
    writePng "out.png" . render $ parsed

